jQuery(document).ajaxSend(function(event, xhr, settings) {
    const safeMethod = method => /^(GET|HEAD|OPTIONS|TRACE)$/.test(method);

    const sameOrigin = url => {
        const host = document.location.host;
        const protocol = document.location.protocol;
        const sr_origin = '//' + host;
        const origin = protocol + sr_origin;
        return (url === origin || url.slice(0, origin.length + 1) === origin + '/') || (url === sr_origin || url.slice(0, sr_origin.length + 1) === sr_origin + '/') || !(/^(\/\/|http:|https:).*/.test(url));
    };

    const getCookie = name => {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            let cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                let cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
        const csrfToken = getCookie('csrftoken');
        if(csrfToken!=null){
            xhr.setRequestHeader("X-CSRFToken", csrfToken);
        }

    }
});
