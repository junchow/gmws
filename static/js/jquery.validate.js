$.extend({
    //是否数字
    isNumber:num=>(/^[0-9]+$/).test(str),
    //是否整数
    isInt:num=>(/^-?\d+$/).test(num),
    //是否正整数，包括首位为0
    isPositiveInt:num=>(/^\d+$/).test(num),
    //是否正整数，不包括首位为0
    isPositiveNum:num=>(/^[1-9]\d*$/).test(num),
    //是否大于0的整数
    isGreaterInt:num=>(/^\d+$/).test(num) ? ((/^0+$/).test(num) ? false : true) : false,
    //是否浮点数
    isFloat:num=>(/^\d+(\.\d+)?$/).test(num),
    //是否非负浮点数
    isPositiveFloat:num=>(/^\d+(\.\d+)?$/).test(num),
    //数组查询
    indexOf:(arr, substr, start)=>{
        if(str != null){
            arr = arr.slice(start);
        }
    },
    //是否字母、数字、下划线
    isNumberLetter:str=>(/^[0-9a-zA-Z\_]+$/).test(str),
    //账号检查：3~20位英文字母、数字、下划线组成，首位非下划线
    isAccount:str=>(/^[a-zA-Z0-9]+[a-zA-Z0-9_]{2, 19}$/).test(str),
    //密码检查：6~20位非中文
    isPassword:str=>(/^[^\u4e00-\u9fa5]{2,16}$/).test(str),
    //是否中文
    isChinese:str=>(/^[\u4e00-\u9fa5]+$/).test(str),
    //是否英文
    isEnglish:str=>(/^[a-zA-Z]+$/).test(str),
    //中文姓名：2~8位中文
    isCnName:str=>(/^[\u4e00-\u9fa5]{2,8}$/).test(str),
    //身份证号码 18位数字和字母组合
    isIdCard:str=>(/^[a-zA-Z0-9]{18, 18}$/).test(str),
    //邮箱地址
    isEmail:str=>(/^([a-zA-Z0-9_])+@([a-zA-Z0-9_-]+[.])+([a-zA-Z]{2,3})$/).test(str),
    //手机号码
    isMobile:str=>(/^(13|14|15|18)[0-9]{9}$/).test(str),
    //电话号码
    isPhone:str=>(/^[0-9]{3,4}\-[0-9]{7,8}$/).test(str),
    //邮政编码
    isPostCode:str=>(/^[1-9][0-9]{5}$/).test(str),
    isZipCode:str=>(/^[a-zA-Z0-9]{3, 12}$/).test(str),
    //URL地址
    isUrl:str=>(/^$[a-zA-Z]+:\/\/[^s]*/).test(str),
    //IP地址
    isIp:str=>(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/).test(str),
    //QQ号码
    isQQ:str=>(/[1-9][0-9]{4,}/).test(str),
    // 日期检查
    isDate:str=>{

    },
    //空字符串验证
    isEmpty:str=>{
        let result = false;
        str = str.trim();
        if(str === "" || str.length === 0){
            return true;
        }
        return result;
    },
    //选择元素并将光标定位到元素上，解决IE focus的bug，使用延迟选择
    selectElement:ele=>setTimeout(()=>{ele.focus(); ele.select();}, 0),
    //验证元素
    validateElement:ele=>{
        const type = $(ele).attr("validate");
        //const error = $(ele).attr("error");
        const val = $(ele).val();

        let result = false;
        switch(type){
            case "account": result = $.isAccount(val); break;
            case "password": result = $.isPassword(val); break;
            default: break;
        }

        return result;
    },
    //验证表单 验证form中中带有validate属性的元素
    validateForm:form=>{
        const eles = form.elements;
        for(let i=0; i<eles.length; i++){
            if(!$(eles[i]).attr("validate")){
                continue;
            }
            let [result, error] = $.validateElement(eles[i]);
        }
        return true;
    },
});