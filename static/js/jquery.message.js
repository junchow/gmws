$.extend({
    message: options => {
        let cssMap = {};
        cssMap[".toast"] = '{top: 50%;left: 50%;position: fixed;-webkit-transform: translate(-50%, -50%);transform: translate(-50%, -50%);border-radius: 0.5rem;background: rgba(0, 0, 0, 0.8);color: #fff;-webkit-box-sizing: border-box;box-sizing: border-box;text-align: center;padding: 10px;z-index: 2000;-webkit-animation-duration: 500ms;animation-duration: 500ms;}';
        cssMap[".toast .text"] = '{max-width: 100%;font-size: 1rem;}';

        const createElement = (tag, option, children = []) => {
            const ele = document.createElement(tag);
            if (option.attrs) {
                for (let key in option.attrs) {
                    ele.setAttribute(key, option.attrs[key]);
                }
            }
            if (option.props) {
                for (let key in option.props) {
                    ele[key] = option.props[key];
                }
            }
            if (children && children.length) {
                for (let i = 0; i < children.length; i++) {
                    if (children[i]) {
                        ele.appendChild(children[i]);
                    }
                }
            }
            return ele;
        };

        const generateCss = (id, cssMap) => {
            if (document.querySelector(id)) {
                return;
            }

            let css = "";
            for (let key in cssMap) {
                css += key + cssMap[key];
            }

            const ele = createElement("style", {attrs: {id: id}, props: {innerHTML: css}});
            document.querySelector("head").appendChild(ele);
        };

        const init = options=>{
            if(!options) return;

            options = typeof options==="string" ? {type:"info", text:options} : options;
            options.type = options.type || "info";

            let iconClass = "";
            switch(options.type){
                case "success": iconClass="fas fa-check"; break;
                case "error": iconClass="fas fa-times"; break;
                case "loading": iconClass="fas fa-spinner fa-pulse"; break;
                default: iconClass=""; break;
            }

            let children = [];
            if(iconClass.length !== 0){
                children.push(createElement("i", {attrs:{class:iconClass}}, []));
            }
            children.push(createElement("p", {attrs:{class:"text"}, props:{innerHTML:options.text}}, []));
            const ele = createElement("div", {attrs:{class:"toast"}}, children);

            const isIE = /(MSIE)|(Trident)|(Edge)/.test(navigator.userAgent);

            const currentEle = document.querySelector(".toast");
            if(currentEle){
                isIE ? currentEle.removeNode() : currentEle.remove();
            }
            document.body.appendChild(ele);

            setTimeout(()=>{
                isIE ? (ele&&ele.removeNode()) : (ele&&ele.remove());
            }, options.timeout || 2e3);
        };

        generateCss("message", cssMap);
        init(options);
    }
});