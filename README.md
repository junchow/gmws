# gmws

## 后端资源
- PEP8 https://legacy.python.org/dev/peps/pep-0008/
- Django https://docs.djangoproject.com/zh-hans/3.1
- https://simpleisbetterthancomplex.com/

## 前端资源
- Tailwindcss https://tailwindcss.cn/
- Tailwind组件 https://tailwindcomponents.com/
- CodePen: https://codepen.io/
- 字体图标 https://fontawesome.cc/
- 随机图片 https://source.unsplash.com
- https://www.bootcdn.cn/
- jQuery https://jquery.com/download/
- jQuery 1.10.3 速查表 https://www.jb51.net/shouce/jquery/cheatsheet.html
- Zepto https://www.runoob.com/manual/zeptojs.html

## 前端类库
- https://cdn.bootcdn.net/ajax/libs/tailwindcss/1.4.6/tailwind.min.css
- https://cdn.bootcdn.net/ajax/libs/font-awesome/5.13.0/js/all.min.js
- https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js

jQuery
- jQuery Message: http://bassistance.de/jquery-plugins/jquery-plugin-message/
- jQuery Growl：https://ksylvest.github.io/jquery-growl/

## 数据库

```sql
CREATE TABLE `web_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `createtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='管理员日志表';
```
