# Generated by Django 3.1.1 on 2020-10-10 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manage', '0002_profilemodel_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profilemodel',
            old_name='createtime',
            new_name='create_time',
        ),
        migrations.RenameField(
            model_name='profilemodel',
            old_name='updatetime',
            new_name='update_time',
        ),
        migrations.AlterField(
            model_name='profilemodel',
            name='remark',
            field=models.CharField(max_length=255, verbose_name='备注说明'),
        ),
    ]
