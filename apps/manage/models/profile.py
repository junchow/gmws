from django.db import models
from django.contrib.auth.models import User

from apps.manage.apps import ManageConfig
from .base import BaseModel


# 用户资料
class ProfileModel(BaseModel):
    # 字段
    auth_user = models.OneToOneField(User, on_delete=models.CASCADE)
    nickname = models.CharField("昵称", max_length=32, default="")
    avatar = models.ImageField("头像", max_length = 255, default="")
    realname = models.CharField("姓名", max_length=32, default="")
    idnumber = models.CharField("身份证号", max_length=32, unique=True, default="")
    phone = models.CharField("手机号码", unique=True, max_length=32, default="")
    email = models.EmailField("电子邮箱", max_length=255, unique=True, default="")

    # 字符串输出
    def __str__(self):
        return self.nickname

    # 元数据内部类
    class Meta:
        app_label= ManageConfig.name
        db_table = "manage_profile"
        verbose_name="用户资料"
        verbose_name_plural="用户资料"