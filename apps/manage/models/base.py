from django.db import models

from apps.manage.apps import ManageConfig


# 抽象模型基类
class BaseModel(models.Model):
    create_time = models.DateTimeField(verbose_name="创建时间", auto_now_add=True)
    update_time = models.DateTimeField(verbose_name="更新时间", auto_now=True)
    deleted = models.BooleanField(verbose_name="删除标记", default=False)
    remark = models.CharField(verbose_name="备注说明", max_length=255)
    status = models.IntegerField(verbose_name="当前状态", default=1)

    class Meta():
        app_label = ManageConfig.name
        abstract = True
        ordering=["-create_time"]