import random
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont

# 自定义模板过滤器
from django.template.defaulttags import register


@register.filter
def sequence(value):
    return range(value)



# 自定义工具类
class Utils:
    # 生成图片验证码
    @staticmethod
    def makeAuthImg(len=4, width=270, height=50):
        # 定义随机颜色生成函数
        def make_random_color(start=0, stop=255):
            r = random.randrange(start, stop)
            g = random.randrange(start, stop)
            b = random.randrange(start, stop)
            return (r, g, b)

        # 定义随机字符生成函数
        def make_random_char():
            return random.choice([
                str(random.randint(0, 9)),
                chr(random.randint(97, 122)),
                chr(random.randint(65, 90))
            ])

        # 创建画布
        canvas = Image.new(
            mode="RGB",
            size=(width, height),
            color=make_random_color(218, 255)
        )
        # 创建画笔
        draw = ImageDraw.Draw(canvas, mode="RGB")
        # 创建字体
        font = ImageFont.truetype(
            font="static/fonts/Mogul-Arial.ttf",
            size=random.randint(int(height/3), height-10)
        )
        # 随机生成字符
        code = ""
        for i in range(len):
            char = make_random_char()
            x = i * width / 4 + random.randint(0, 30)
            y = random.randint(0, int(height/3))
            draw.text(
                (x, y),
                char,
                make_random_color(128, 192),
                font
            )
            code += char
        # 随机干扰线
        for i in range(len):
            x = random.randint(0, int(width/6))
            y = random.randint(0, int(height/2))
            draw.arc(
                (x, y, width-x, height-y),
                0,
                180,
                make_random_color(64, 128)
            )
        # 随机干扰点
        for i in range(len*50):
            draw.point(
                (random.randint(0, width), random.randint(0, height)),
                fill=make_random_color(0, 64)
            )
        return canvas, code