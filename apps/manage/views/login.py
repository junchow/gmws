from io import BytesIO

from django.contrib import auth
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic.base import View

from apps.manage.utils import Utils

# 登录
class Login(View):
    template_name = "login.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})
    def get(self, request):
        return render(request, self.template_name)
    def post(self, request):
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        authcode = request.POST.get("authcode", None)
        # print(username, password, authcode.lower(), request.session["authcode"])
        # 图片验证码
        if not authcode:
            return self.error(request, "请填写验证码")
        # 验证码判断
        if authcode.lower() != request.session["authcode"]:
            return self.error(request, "验证码输入有误")
        # 输入判断
        if not username or not password:
            return self.error(request, "请填写账号或密码")
        # 使用auth模块去auth_user表查找
        user = auth.authenticate(username=username, password=password)
        if not user:
            return self.error(request, "账号或密码输入有误")
        # 执行登录
        auth.login(request, user)
        # 跳转首页
        return redirect("manage:home")


# 生成随机图片验证码
def authimg(request):
    fd = BytesIO()
    # 生成随机图片二维码
    im,code = Utils.makeAuthImg()
    # 保存图片格式
    im.save(fd, "PNG")
    # 保存验证码 统一转化为小写
    request.session["authcode"] = code.lower()
    # 生成图片
    return HttpResponse(fd.getvalue())