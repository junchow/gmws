import logging

from django.contrib import auth
from django.shortcuts import render, redirect

logger = logging.getLogger("log")

# 首页
def index(request):
    template_name = 'home.html'
    # 判断用户是否登录
    if(request.user.is_authenticated == False):
        return redirect("manage:logout")
    # 渲染模板
    return render(request, template_name)

# 退出
def logout(request):
    auth.logout(request)
    return redirect("manage:login")