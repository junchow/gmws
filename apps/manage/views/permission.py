from django.contrib.auth.models import Permission
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from django.views.generic.base import View

from apps.manage import configs


class PermissionList(ListView):
    model = Permission
    template_name = "permission/list.html"
    context_object_name = "object_list"
    paginate_by = configs.PAGINATE_BY
    http_method_name = ["get"]

    def get_paginate_by(self, queryset):
        paginate_by = self.request.GET.get("paginate_by")
        if not paginate_by:
            paginate_by = self.paginate_by
        return paginate_by

    def get_queryset(self):
        queryset_list = Permission.objects.all()

        return queryset_list

    def get_context_data(self, **kwargs):
        context = super(PermissionList, self).get_context_data(**kwargs)
        return context


class PermissionDetail(DetailView):
    model = Permission
    template_name = "permission/detail.html"
    context_object_name = "object"

    def get(self, request, *args, **kwargs):
        response = super(PermissionDetail, self).get(request, *args, **kwargs)
        return response

    def get_object(self, queryset=None):
        object = super(PermissionDetail, self).get_object(queryset=None)
        return object

    def get_context_data(self, **kwargs):
        context = super(PermissionDetail, self).get_context_data(**kwargs)
        return context


class PermissionCreate(View):
    template_name = "permission/create.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request):
        context = dict()
        return render(request, self.template_name, context)

    def post(self, request):
        name = request.POST.get("name")
        if not name:
            return self.error(request, "请填写权限名称")

        codename = request.POST.get("codename")
        if not codename:
            return self.error(request, "请填写代码标识")

        content_type_id = request.POST.get("content_type_id")
        if not content_type_id:
            return self.error(request, "请填写内容类型")

        count = Permission.objects.filter(codename=codename).count()
        if count > 0:
            return self.error(request, "代码标识已使用")

        params = dict()
        params["name"] = name
        params["codename"] = codename
        params["content_type_id"] = content_type_id
        obj = Permission.objects.create(**params)
        if not obj:
            return self.error(request, "创建失败")

        return redirect("manage:permission")
