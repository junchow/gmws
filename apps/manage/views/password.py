import sys

from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic.base import View

from apps.manage.models import ProfileModel

class PasswordView(View):

    template_name = "password.html"

    def get(self, request):

        args = dict()
        # 获取登录用户的主键
        auth_user_id = request.session["_auth_user_id"]
        if not auth_user_id:
            messages.error(request, "登录过期 请重新登录！")

        # 获取用户资料
        try:
            result = ProfileModel.objects.get(auth_user_id=auth_user_id)
            if result:
                args["avatar"] = result.avatar
                args["nickname"] = result.nickname
                args["realname"] = result.realname
                args["idnumber"] = result.idnumber
                args["email"] = result.email
                args["phone"] = result.phone
                args["remark"] = result.remark
        except ProfileModel.DoesNotExist:
            print("except: profile does not exist")

        print(args)
        # 渲染模板
        return render(request, self.template_name, args)

    def post(self, request):
        auth_user_id = request.session["_auth_user_id"]
        if not auth_user_id:
            messages.error(request, "登录过期 请重新登录！")

        nickname = request.POST.get("nickname", None)
        realname = request.POST.get("realname", None)
        idnumber = request.POST.get("idnumber", None)
        email = request.POST.get("email", None)
        phone = request.POST.get("phone", None)
        remark = request.POST.get("remark", None)

        defaults = dict()
        if nickname: defaults["nickname"] = nickname
        if realname: defaults["realname"] = realname
        if idnumber: defaults["idnumber"] = idnumber
        if email: defaults["email"] = email
        if phone: defaults["phone"] = phone
        if remark: defaults["remark"] = remark
        print(defaults)

        obj, created = ProfileModel.objects.update_or_create(defaults=defaults, auth_user_id=auth_user_id)
        if created:
            print("创建成功 更新失败")

        return redirect("manage:password")