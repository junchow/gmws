import datetime
import json
import os
import time
import re
from io import BytesIO

from django.core import serializers


import xlwt as xlwt
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User, Group
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views.generic import ListView
from django.views.generic.base import View

from apps.manage import configs


class UserList(ListView):
    template_name = "user/list.html"
    context_object_name = "pagelist"
    paginate_by = configs.PAGINATE_BY
    http_method_names = ["get"]

    # 重写分页判断
    def get_paginate_by(self, queryset):
        paginate_by = self.request.GET.get("paginate_by")
        if paginate_by:
            return paginate_by
        return self.paginate_by

    # 获取上下文对象对应的查询结果集
    def get_queryset(self):
        # 获取GET参数
        # get_object_or_404(User, pk=self.kwargs.get("pk"))
        querySet = User.objects.all()

        orderby = self.request.GET.get("orderby", "last_login")
        sort = self.request.GET.get("sort", "desc")
        if not orderby:
            orderby = "last_login"
        if not sort:
            sort = "desc"

        if sort == "desc":
            order_by = "-{0}".format(orderby)
        elif sort == "asc":
            order_by = "{0}".format(orderby)
        else:
            order_by = "{0}".format(orderby)
        querySet = querySet.order_by(order_by)

        id = self.request.GET.get("id")
        if id:
            querySet = querySet.filter(id = id)

        username = self.request.GET.get("username")
        if username:
            querySet = querySet.filter(username = username)

        email = self.request.GET.get("email")
        if email:
            querySet = querySet.filter(email = email)

        date_joined = self.request.GET.get("date_joined")
        if date_joined:
            year, month, day = time.strptime(date_joined, "%Y-%m-%d")[:3]
            querySet = querySet.filter(date_joined__year = year, date_joined__month = month, date_joined__day = day)

        last_login = self.request.GET.get("last_login")
        if last_login:
            year, month, day = time.strptime(last_login, "%Y-%m-%d")[:3]
            querySet = querySet.filter(last_login__year = year, last_login__month = month, last_login__day = day)

        for item in querySet:
            item.create_time = timezone.now().strftime("%Y-%m-%d %H:%M:%S")

        return querySet

    # 获取上下文对象数据
    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data(**kwargs)

        context["group_list"] = Group.objects.all().order_by("name")

        context["paginate_by"] = self.paginate_by
        context["orderby"] = self.request.GET.get("orderby", "last_login")
        context["sort"] = self.request.GET.get("sort", "desc")

        context["id"] = self.request.GET.get("id", "")
        context["username"] = self.request.GET.get("username", "")
        context["email"] = self.request.GET.get("email", "")
        context["date_joined"] = self.request.GET.get("date_joined", "")
        context["last_login"] = self.request.GET.get("last_login", "")

        return context


class UserCreate(View):
    template_name = "user/create.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request):
        context = dict()

        return render(request, self.template_name, context)

    def post(self, request):
        username = request.POST.get("username","")
        password = request.POST.get("password","")
        confirm_password = request.POST.get("confirm_password","")
        last_name = request.POST.get("last_name","")
        first_name = request.POST.get("first_name","")
        email = request.POST.get("email","")
        is_superuser = request.POST.get("is_superuser", False)
        is_staff = request.POST.get("is_staff", False)
        is_active = request.POST.get("is_active", False)
        print(request.POST)

        if is_superuser == "" or len(is_superuser) == 0:
            is_superuser = False
        if is_staff == "" or len(is_staff) == 0:
            is_staff = False
        if is_active == "" or len(is_active) == 0:
            is_active = False

        if not username:
            return self.error(request, "请填写账户")

        username = username.strip()
        if not len(username) > 3 and len(username) < 20:
            return self.error(request, "账户长度3~20位")

        if not re.match("^[a-zA-Z0-9_-]{3,20}$", username):
            return self.error(request, "账号需由字母、数字、下划线构成")

        cnt = User.objects.filter(username=username).count()
        if cnt > 0:
            return self.error(request, "账号已存在")

        if not password:
            return self.error(request, "请填写密码")

        password = password.strip()
        if not len(password) > 6:
            return self.error(request, "密码不得少于6位")

        if not re.match("^[a-zA-Z0-9_-]{6,}$", password):
            return self.error(request, "密码必须由数字、字母、下划线组合构成")

        if not confirm_password:
            return self.error(request, "请填写重复密码")

        confirm_password = confirm_password.strip()
        if not len(confirm_password) > 6:
            return self.error(request, "重复密码不得少于6位")

        if not re.match("^[a-zA-Z0-9_-]{6,}$", confirm_password):
            return self.error(request, "重复密码必须由数字、字母、下划线组合构成")

        if not password == confirm_password:
            return self.error(request, "重复密码输入错误")

        password = make_password(password)

        if not email:
            return self.error(request, "请填写邮箱")

        email = email.strip()
        if len(email) > 320:
            return self.error(request, "邮箱长度不得超过320位")

        if not re.match("\w[-\w.+]*@([a-zA-Z0-9][a-zA-Z0-9-]+\.)+[a-zA-Z]{2,14}", email):
            return self.error(request, "邮箱格式错误")

        cnt = User.objects.filter(email=email).count()
        if cnt > 0:
            return self.error(request, "邮箱已存在")

        last_name = last_name.strip()
        first_name = first_name.strip()

        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d %H:%I:%S")
        date_joined = current_datetime
        last_login = current_datetime

        params = dict()
        params["username"] = username
        params["password"] = password
        params["last_name"] = last_name
        params["first_name"] = first_name
        params["email"] = email
        params["is_superuser"] = is_superuser
        params["is_staff"] = is_staff
        params["is_active"] = is_active
        params["date_joined"] = date_joined
        params["last_login"] = last_login
        print(params)
        result = User.objects.create(**params)
        if not result:
            return self.error(request, "创建失败")

        return redirect("manage:user")


class UserDetail(View):
    template_name = "user/detail.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        context = dict()
        if not pk:
            return self.error(request, "参数缺失")
        try:
            result = User.objects.get(id=pk)
            if not result:
                return self.error(request, "暂无数据")
            context["info"] = result
        except User.DoesNotExist:
            print("except: user model does not exist")
        return render(request, self.template_name, context)


class UserUpdate(View):
    template_name = "user/update.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        context = dict()
        if not pk:
            return self.error(request, "参数缺失")
        try:
            result = User.objects.get(id=pk)
            if not result:
                return self.error(request, "暂无数据")
            context["info"] = result
        except User.DoesNotExist:
            print("except: user model does not exist")
        return render(request, self.template_name, context)

    def post(self, request, pk):
        username = request.POST.get("username").strip()
        email = request.POST.get("email").strip()
        last_name = request.POST.get("last_name").strip()
        first_name = request.POST.get("first_name").strip()

        if not username or not email:
            return self.error(request, "请填写必填项")

        cnt = User.objects.filter(id=pk).count()
        if cnt == 0:
            return self.error(request, "数据不存在")

        cnt = User.objects.exclude(id=pk).filter(email=email).count()
        if cnt > 0:
            return self.error(request, "邮箱已使用")

        params = dict()
        params["username"] = username
        params["email"] = email
        params["last_name"] = last_name
        params["first_name"] = first_name
        result = User.objects.filter(id=pk).update(**params)
        if not result:
            return self.error(request, "操作失败")

        return redirect("manage:user")


class UserPassword(View):
    template_name = "user/password.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        context = dict()
        if not pk:
            return self.error(request, "参数缺失")
        context["pk"] = int(pk)

        try:
            cnt = User.objects.filter(id=pk).count()
            if cnt == 0:
                return self.error(request, "暂无数据")
        except User.DoesNotExist:
            print("except: user model does not exist")
        return render(request, self.template_name, context)

    def post(self, request, pk):
        original_password = request.POST.get("original_password")
        current_password = request.POST.get("current_password")
        confirm_password = request.POST.get("confirm_password")

        original_password = original_password.strip()
        current_password = current_password.strip()
        confirm_password = confirm_password.strip()

        if not original_password or not current_password or not confirm_password:
            return self.error(request, "请输入必填项")

        if current_password != confirm_password:
            return self.error(request, "两次密码输入有误")

        password = make_password(original_password)
        cnt = User.objects.filter(id=pk, password=password).count()
        if cnt == 0:
            return self.error(request, "原始密码输入有误")

        result = User.objects.filter(id=pk).update(password=password)
        if not result:
            return self.error(request, "操作失败")

        return redirect("manage:user")


def toggle(request):
    if request.method.upper() != "POST":
        return JsonResponse({"code":500, "message":"请求方式错误"})

    pk = request.POST.get("pk")
    field = request.POST.get("field")
    value = request.POST.get("value")

    if not pk or not field or not value:
        return JsonResponse({"code":500, "message":"参数错误"})

    cnt = User.objects.filter(id=pk).count()
    if cnt == 0:
        return JsonResponse({"code": 500, "message": "记录不存在"})

    params = dict()
    params[field] = int(value)
    result = User.objects.filter(id=pk).update(**params)
    if not result:
        return JsonResponse({"code":500, "message":"切换失败"})

    return JsonResponse({"code":200, "message":"操作成功"})


class UserDelete(View):
    def post(self, request):
        id = request.POST.get("id")
        if not id:
            return JsonResponse({"code": 500, "message": "参数缺失"})

        if "," in id:
            for item in id.split(","):
                print(item)
        else:
            cnt = User.objects.filter(id=id).count()
            if cnt == 0:
                return JsonResponse({"code": 500, "message": "暂无数据"})

        return JsonResponse({"code": 200, "message": "操作成功"})


class UserExcel(View):
    def get(self, request):
        condition = dict()
        id = request.GET.get("id", "")
        if id:
            condition["id"] = id
        username = request.GET.get("username", "")
        if username:
            condition["username"] = username
        email = request.GET.get("email", "")
        if email:
            condition["email"] = email
        date_joined = request.GET.get("date_joined", "")
        if date_joined:
            year, month, day = time.strptime(date_joined, "%Y-%m-%d")[:3]
            condition["date_joined__year"] = year
            condition["date_joined__month"] = month
            condition["date_joined__day"] = day
        last_login = request.GET.get("last_login", "")
        if last_login:
            year, month, day = time.strptime(last_login, "%Y-%m-%d")[:3]
            condition["last_login__year"] = year
            condition["last_login__month"] = month
            condition["last_login__day"] = day
        results = User.objects.filter(**condition).all()

        sheet_name = "ws1"
        fields = [
            {"field":"id", "name":"编号", "type":"string"},
            {"field":"username", "name":"账号", "type":"string"},
            {"field":"last_name", "name":"姓氏", "type":"string"},
            {"field":"first_name", "name":"名字", "type":"string"},
            {"field":"email", "name":"邮箱", "type":"string"},
            {"field":"date_joined", "name":"加入时间", "type":"datetime"},
        ]

        # 创建工作簿
        workbook = xlwt.Workbook(encoding="utf-8")
        # 添加工作单
        worksheet = workbook.add_sheet(sheet_name)
        # 写入表头
        for index in range(len(fields)):
            item = fields[index]
            worksheet.write(0, index, item["name"])
        # 写入数据
        values = results.values()
        for index in range(len(values)):
            for idx in range(len(fields)):
                field = fields[idx]["field"]
                type = fields[idx]["type"]
                value = values[index][field]
                if type == "datetime":
                    value = value.strftime("%Y-%m-%d %H:%M:%S")
                worksheet.write(index+1, idx, value)

        # timestamp = int(round(time.time()*1000))
        now = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        filename = "{0}.xls".format(now)
        filepath = filename
        if os.path.exists(filepath):
            os.remove(filepath)

        bio = BytesIO()
        workbook.save(bio)
        bio.seek(0)

        response = HttpResponse(content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = "attachment; filename={filename}".format(filename=filename)
        response.write(bio.getvalue())
        return response


class UserGroup(View):
    template_name = "user/group.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        context = dict()
        context["pk"] = pk
        context["group"] = Group.objects.all()

        user = User.objects.get(id=pk)
        context["user_groups_id_list"] = user.groups.values_list("id", flat=True)

        return render(request, self.template_name, context)

    def post(self, request, pk):
        data = dict()
        data["user_id"] = int(pk)
        data["group"] = list(Group.objects.values("id", "name"))

        user = User.objects.get(id=pk)
        data["group_id"] = list(user.groups.values_list("id", flat=True))
        return JsonResponse({"code": 200, "message": "操作成功", "data":data})


def user_group_toggle(request):
    byte_body = request.body
    str_body = str(byte_body, encoding="utf8")
    dict_body = json.loads(str_body)

    user_id = dict_body.get("user_id")
    group_id = dict_body.get("group_id")
    if user_id is None or group_id is None:
        return JsonResponse({"code":500, "message":"参数错误"})

    user = User.objects.get(id=user_id)
    if not user:
        return JsonResponse({"code":500, "message":"用户不存在"})

    group = Group.objects.get(id=group_id)
    if not group:
        return JsonResponse({"code":500, "message":"群组不存在"})

    print(user.groups.filter(id=group_id).exists())

    exists = user.groups.filter(id=group_id).exists()
    if exists:
        result = user.groups.remove(group)
    else:
        result = user.groups.add(group)

    if result is not None:
        return JsonResponse({"code":500, "message":"操作失败"})

    return JsonResponse({"code": 200, "message": "操作成功"})