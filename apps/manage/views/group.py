import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DeleteView, ListView
from django.views.generic.base import View

from apps.manage import configs


class GroupList(ListView):
    model = Group
    template_name = "group/list.html"
    context_object_name = "object_list"
    paginate_by = configs.PAGINATE_BY
    http_method_names = ["get"]

    def get_paginate_by(self, queryset):
        paginate_by = self.request.GET.get("paginate_by")
        if not paginate_by:
            paginate_by = self.paginate_by
        return paginate_by

    def get_queryset(self):
        queryset_list = Group.objects.all()

        orderby = self.request.GET.get("orderby", "id")
        sort = self.request.GET.get("sort", "desc")
        if orderby and sort:
            order_by = "-{0}".format(orderby)
            if sort == "asc":
                order_by = "{0}".format(orderby)
            queryset_list = queryset_list.order_by(order_by)

        id = self.request.GET.get("id")
        if id:
            queryset_list = queryset_list.filter(id=id)

        name = self.request.GET.get("name")
        if name:
            queryset_list = queryset_list.filter(name__contains=name)

        return queryset_list

    def get_context_data(self, **kwargs):
        context = super(GroupList, self).get_context_data(**kwargs)
        context["id"] = self.request.GET.get("id")
        context["name"] = self.request.GET.get("name","")
        return context


class GroupCreate(View):
    template_name = "group/create.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request):
        ctx = {}
        return render(request, self.template_name, ctx)

    def post(self, request):
        name = request.POST.get("name")
        sort = request.POST.get("sort")
        remark = request.POST.get("remark")
        if not name:
            return self.error(request, "请填写角色名称")
        # 判断名称是否已经使用
        try:
            result = Group.objects.filter(name=name)
            print(result)
            if result:
                return self.error(request, "名称已存在")
        except Group.DoesNotExist:
            print("except: group model does not exist") # todo: debug
            # messages.info(request, "Group 模型不存在")
        # 添加数据
        obj = Group.objects.create(name=name)
        # print(obj)
        return redirect("manage:group")


class GroupUpdate(View):
    template_name = "group/update.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        ctx = dict()

        if not pk:
            return self.error(request, "参数缺失")

        try:
            result = Group.objects.get(id=pk)
            if not result:
                return self.error(request, "暂无数据")
            ctx["group"] = result
        except Group.DoesNotExist:
            print("except: group model does not exist")

        return render(request, self.template_name, ctx)

    def post(self, request, pk):
        if not pk:
            return self.error(request, "参数缺失")

        name = request.POST.get("name")
        if not name:
            return self.error(request, "请填写名称")

        result = Group.objects.filter(id=pk).update(name=name)
        if not result:
            return self.error(request, "修改失败")

        return redirect("manage:group")


class GroupDelete(DeleteView):
    def post(self, request, *args, **kwargs):
        pk = self.kwargs["pk"]
        if not pk:
            return JsonResponse({"code": 400, "message": "参数缺失"})

        result = Group.objects.get(id = pk)
        result.delete()

        return JsonResponse({"code":200, "message":"删除成功"})


class GroupPermission(View):
    template_name = "group/permission.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        ctx = dict()
        ctx["pk"] = pk
        ctx["content_type"] = ContentType.objects.all()
        ctx["permission"] = Permission.objects.all().order_by("content_type_id")

        return render(request, self.template_name, ctx)

    def post(self, request, pk):
        content_type = ContentType.objects.values("id", "app_label", "model")
        permission = Permission.objects.order_by("content_type_id").values("id", "content_type_id", "name", "codename")

        data = dict()
        data["content_type"] = list(content_type)
        data["permission"] = list(permission)

        return JsonResponse({"code":200, "message":"success", "data":data})
