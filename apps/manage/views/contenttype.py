from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.generic import ListView, DetailView
from django.views.generic.base import View

from apps.manage import configs


class ContenttypeList(ListView):
    model = ContentType
    template_name = "contenttype/list.html"
    context_object_name = "object_list"
    # paginate_by = configs.PAGINATE_BY
    paginate_by = 100
    http_method_names = ["get"]

    def get_paginate_by(self, queryset):
        paginate_by = self.request.GET.get("paginate_by")
        if not paginate_by:
            paginate_by = self.paginate_by
        return paginate_by

    def get_queryset(self):
        queryset_list = ContentType.objects.all().order_by("id")
        id = self.request.GET.get("id")
        if id:
            queryset_list = queryset_list.filter(id=id)
        app_label = self.request.GET.get("app_label")
        if app_label:
            queryset_list = queryset_list.filter(app_label=app_label)
        model = self.request.GET.get("model")
        if model:
            queryset_list = queryset_list.filter(model=model)
        return queryset_list

    def get_context_data(self, **kwargs):
        context = super(ContenttypeList, self).get_context_data(**kwargs)
        context["id"] = self.request.GET.get("id")
        context["app_label"] = self.request.GET.get("app_label", "")
        context["model"] = self.request.GET.get("model", "")
        return context


class ContenttypeDetail(DetailView):
    model = ContentType
    template_name = "contenttype/detail.html"
    context_object_name = "object"

    def get(self, request, *args, **kwargs):
        response = super(ContenttypeDetail, self).get(request, *args, **kwargs)
        return response

    def get_object(self, queryset=None):
        object = super(ContenttypeDetail, self).get_object(queryset)
        return object

    def get_context_data(self, **kwargs):
        context = super(ContenttypeDetail, self).get_context_data(**kwargs)
        return context


class ContenttypeUpdate(View):
    template_name = "contenttype/update.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request, pk):
        if not pk:
            return self.error(request, "参数缺失")

        queryset_list = ContentType.objects.filter(id=pk)
        if queryset_list.count() == 0:
            return self.error(request, "暂无数据")

        context = dict()
        context["object"] = queryset_list.get()
        return render(request, self.template_name, context)

    def post(self, request, pk):
        if not pk:
            return self.error(request, "非法请求禁止访问")

        app_label = request.POST.get("app_label")
        if not app_label:
            return self.error(request, "请填写应用标识")

        model = request.POST.get("model")
        if not model:
            return self.error(request, "请填写模型名称")

        params = dict()
        params["app_label"] = app_label
        params["model"] = model
        count = ContentType.objects.filter(**params).count()
        if count > 0:
            return self.error(request, "应用标识与模型名称已存在")

        object = ContentType.objects.filter(id=pk).update(**params)
        if not object:
            return self.error(request, "操作失败")

        return redirect("manage:contenttype")


class ContenttypeCreate(View):
    template_name = "contenttype/create.html"

    def error(self, request, message=""):
        return render(request, self.template_name, {"error":message})

    def get(self, request):
        context = dict()
        return render(request, self.template_name, context)

    def post(self, request):
        app_label = request.POST.get("app_label")
        if not app_label:
            return self.error(request, "请填写应用标识")

        model = request.POST.get("model")
        if not model:
            return self.error(request, "请填写模型名称")

        params = dict()
        params["app_label"] = app_label
        params["model"] = model
        count = ContentType.objects.filter(**params).count()
        if count > 0:
            return self.error(request, "应用标识与模型名称已存在")

        object = ContentType.objects.create(**params)
        if not object:
            return self.error(request, "操作失败")

        return redirect("manage:contenttype")


class ContenttypeDelete(View):
    def error(self, message="", code=500):
        return JsonResponse({"code":code, "message":message})

    def success(self, message="", code=200):
        return JsonResponse({"code":code, "message":message})

    def post(self, request):
        pk = request.POST.get("pk")

        if not pk:
            return self.error("非法请求禁止访问")

        if ',' in pk:
            in_list = pk.split(",")
            query_set_list = ContentType.objects.filter(id__in = in_list)
        else:
            query_set_list = ContentType.objects.filter(id=pk)

        result = query_set_list.delete()
        if not result:
            return self.error("删除失败")

        return self.success("删除成功")

