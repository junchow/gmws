from django.conf.urls import url
from django.urls import path, re_path

from apps.manage.apps import ManageConfig
from .views import login, home, profile, password, group, user, permission, contenttype

app_name = ManageConfig.name

urlpatterns = [
    # home
    path("", home.index, name="home"),
    re_path(r"^logout/$", home.logout, name="logout"),
    # login
    re_path(r"^login/$", login.Login.as_view(), name="login"),
    re_path(r"^authimg/$", login.authimg, name="authimg"),
    # profile
    # re_path(r"^profile/", profile.index, name="profile"),
    re_path(r"^profile/$", profile.ProfileView.as_view(), name="profile"),
    re_path(r"^password/avatar/$", profile.avatar, name="avatar"),
    re_path(r"^password/$", password.PasswordView.as_view(), name="password"),
    # group
    url(r"^group/$", group.GroupList.as_view(), name="group"),
    re_path(r"^group/create/$", group.GroupCreate.as_view(), name="group_create"),
    url("^group/update/(?P<pk>\d+)/$", group.GroupUpdate.as_view(), name="group_update"),
    path("group/delete/<int:pk>", group.GroupDelete.as_view(), name="group_delete"),
    url("^group/permission/(?P<pk>\d+)$", group.GroupPermission.as_view(), name="group_permission"),
    # user
    url("^user/$", user.UserList.as_view(), name="user"),
    url("^user/toggle/$", user.toggle, name="user_toggle"),
    url("^user/create/$", user.UserCreate.as_view(), name="user_create"),
    url("^user/update/(?P<pk>\d+)/$", user.UserUpdate.as_view(), name="user_update"),
    url("^user/detail/(?P<pk>\d+)/$", user.UserDetail.as_view(), name="user_detail"),
    url("^user/password/(?P<pk>\d+)/$", user.UserPassword.as_view(), name="user_password"),
    url("^user/group/(?P<pk>\d+)/$", user.UserGroup.as_view(), name="user_group"),
    url("^user/group/toggle/$", user.user_group_toggle, name="user_group_toggle"),
    url("^user/delete/$", user.UserDelete.as_view(), name="user_delete"),
    url("^user/excel/$", user.UserExcel.as_view(), name="user_excel"),
    # contenttype
    url("^contenttype/$", contenttype.ContenttypeList.as_view(), name="contenttype"),
    url("^contenttype/detail/(?P<pk>\d+)/$", contenttype.ContenttypeDetail.as_view(), name="contenttype_detail"),
    url("^contenttype/update/(?P<pk>\d+)/$", contenttype.ContenttypeUpdate.as_view(), name="contenttype_update"),
    url("^contenttype/create/$", contenttype.ContenttypeCreate.as_view(), name="contenttype_create"),
    url("^contenttype/delete/$", contenttype.ContenttypeDelete.as_view(), name="contenttype_delete"),
    # permission
    url("^permission/$", permission.PermissionList.as_view(), name="permission"),
    url("^permission/detail/(?P<pk>\d+)/$", permission.PermissionDetail.as_view(), name="permission_detail"),
    url("^permission/create/$", permission.PermissionCreate.as_view(), name="permission_create"),
]