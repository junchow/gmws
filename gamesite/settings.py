import os
import sys
import time
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

# 将apps路径添加到Python导入模块的搜索路径下
sys.path.insert(0, os.path.join(BASE_DIR, "apps"))

# 调试模式
DEBUG = True


SECRET_KEY = '&#&+==e=^u(65-%y2l8b=sum-gwuj@515^jc_l2b8*4g&^_af-'

ROOT_URLCONF = 'gamesite.urls'

WSGI_APPLICATION = 'gamesite.wsgi.application'



# 语言
LANGUAGE_CODE = 'zh-hans'
USE_I18N = True
USE_L10N = True

# 时区
TIME_ZONE = 'Asia/Shanghai'
USE_TZ = False


# 项目静态文件夹
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static")
]

# 应用静态文件夹
STATIC_URL = '/static/'

# 访问白名单
ALLOWED_HOSTS = [

]

# 注册应用
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.manage.apps.ManageConfig',
    'rest_framework'
]

# 中间件
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# 模板配置
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# 数据库
DATABASES = {
    # 默认数据库
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'NAME': 'game',
        'USER': 'root',
        'PASSWORD': 'root',
        'OPTIONS': {
            'init_command': 'SET sql_mode="STRICT_TRANS_TABLES"',
            'charset': 'utf8mb4'
        }
    }
}

# 密码校验器
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.logon.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.logon.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.logon.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.logon.password_validation.NumericPasswordValidator',
    },
]

# 授权用户模型
# AUTH_USER_MODEL = 'manage.User'

# 日志器配置
current_path = os.path.dirname(os.path.realpath(__file__)) # 当前目录的绝对路径
log_path = os.path.join(os.path.dirname(current_path), 'logs') # 订单日志存放目录
if not os.path.exists(log_path): os.mkdir(log_path) # 若目录不存在则创建

LOGGING = {
    'version':1,# 版本
    'disable_existing_loggers':False, # 是否禁用已存在的日志器
    # 日志格式
    'formatters': {
        'standard':{
            'format':'[%(asctime)s] [%(filename)s:%(lineno)d] [%(module)s:%(funcName)s] '
                     '[%(levelname)s]- %(message)s'
        },
        'simple':{
            'format':'%(levelname)s %(module)s %(lineno)d %(message)s'
        },
        'verbose':{
            'format':'%(levelname)s %(asctime)s %(module)s %(lineno)d %(message)s'
        }
    },
    # 过滤器
    'filters': {
        'require_debug_true':{
            '()':'django.utils.log.RequireDebugTrue'
        }
    },
    # 处理器
    'handlers': {
        # 默认记录所有日志
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(log_path, 'all-{}.log'.format(time.strftime('%Y-%m-%d'))),
            'maxBytes': 1024 * 1024 * 5,  # 文件大小
            'backupCount': 5,  # 备份数
            'formatter': 'standard',  # 输出格式
            'encoding': 'utf-8',  # 设置默认编码，否则打印出来汉字乱码
        },
        # 输出错误日志
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(log_path, 'error-{}.log'.format(time.strftime('%Y-%m-%d'))),
            'maxBytes': 1024 * 1024 * 5,  # 文件大小
            'backupCount': 5,  # 备份数
            'formatter': 'standard',  # 输出格式
            'encoding': 'utf-8',  # 设置默认编码
        },
        # 输出info日志
        'info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(log_path, 'info-{}.log'.format(time.strftime('%Y-%m-%d'))),
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': 'utf-8',  # 设置默认编码
        },
        # 控制台输出
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },

    },
    # 配置日志处理器
    'loggers': {
        'django': {
            'handlers': ['default', 'console'],
            'level': 'INFO',  # 日志器接收的最低日志级别
            'propagate': True,
        },
        # log 调用时需要当作参数传入
        'log': {
            'handlers': ['error', 'info', 'console', 'default'],
            'level': 'INFO',
            'propagate': True
        },
    }
}